const puppeteer = require('puppeteer-core');
(async() => {
  const browser = await puppeteer.launch({
    product: 'firefox',
    headless: true,
    executablePath: "/usr/bin/firefox",
    defaultViewport: { width: 1700, height: 800 },
    args: [
    ]
});

const page = await browser.newPage();

await page.goto('https://www.google.com/');
console.log('saving google screenshot');
await page.screenshot({ path: 'screenshots/google.png', fullPage: true });

await page.goto('https://www.digg.com/');
console.log('saving digg screenshot');
await page.screenshot({ path: 'screenshots/digg.png', fullPage: true });

await browser.close();

})();