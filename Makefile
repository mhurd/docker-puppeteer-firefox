SHELL := /bin/bash

.DEFAULT: freshInstall
.PHONY: freshInstall
freshInstall:
	make build
	make run

.PHONY: build
build:
	docker build --progress=plain -t puppeteer-firefox .

.PHONY: run
run:
	docker run -i -v "$$(pwd)/screenshots:/home/app/screenshots" --init --rm --cap-add=SYS_ADMIN --name puppeteer-firefox puppeteer-firefox node firefox.js