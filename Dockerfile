FROM node:16

# examples: https://github.com/puppeteer/puppeteer/blob/main/docker/Dockerfile

RUN apt-get update \
    && apt-get install -y wget gnupg firefox-esr fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-khmeros fonts-kacst fonts-freefont-ttf libxss1

ENV PUPPETEER_PRODUCT=firefox

WORKDIR /home/app

COPY ./package.json /home/app
COPY ./firefox.js /home/app

RUN npm install
RUN mkdir screenshots

CMD echo “Hello World”